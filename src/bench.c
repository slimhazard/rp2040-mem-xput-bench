/**
 * Copyright (c) 2021 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "hardware/dma.h"
#include "hardware/irq.h"
#include "tusb.h"

#define MAX_SCALE (16)
#define TEST_RUNS (100)

static uint8_t src[1U << MAX_SCALE] = { 0 };
static uint8_t dst[1U << MAX_SCALE] = { 0 };

static volatile bool dma_done;
static volatile absolute_time_t dma_to;
static unsigned chan8, chan32;

static void
dma8_isr(void)
{
	dma_to = get_absolute_time();
	dma_channel_acknowledge_irq0(chan8);
	dma_done = true;
}

static void
dma32_isr(void)
{
	dma_to = get_absolute_time();
	dma_channel_acknowledge_irq1(chan32);
	dma_done = true;
}

static bool
check(int xfer_cnt)
{
	for (int i = 0; i < xfer_cnt; i++)
		if (src[i] != dst[i])
			return (false);
	return (true);
}

static void
overwrite(int xfer_cnt)
{
	for (int i = 0; i < xfer_cnt; i++)
		src[i] = (uint8_t) rand();
}

int
main()
{
	absolute_time_t from, to;

	/* 1-byte DMA transfer width */
	chan8 = dma_claim_unused_channel(true);
	/* default_config() sets read_increment=true. */
	dma_channel_config cfg8 = dma_channel_get_default_config(chan8);
	channel_config_set_write_increment(&cfg8, true);
	channel_config_set_transfer_data_size(&cfg8, DMA_SIZE_8);
	dma_channel_set_config(chan8, &cfg8, false);

	/* Raise DMA_IRQ_0 when 1-byte-wide transfers complete. */
	irq_set_exclusive_handler(DMA_IRQ_0, dma8_isr);
	irq_set_enabled(DMA_IRQ_0, true);
	dma_set_irq0_channel_mask_enabled(1U << chan8, true);

	/* 4-byte DMA transfer width */
	chan32 = dma_claim_unused_channel(true);
	dma_channel_config cfg32 = dma_channel_get_default_config(chan32);
	channel_config_set_write_increment(&cfg32, true);
	channel_config_set_transfer_data_size(&cfg32, DMA_SIZE_32);
	dma_channel_set_config(chan32, &cfg32, false);

	/* Raise DMA_IRQ_1 for 4-byte-wide transfers. */
	irq_set_exclusive_handler(DMA_IRQ_1, dma32_isr);
	irq_set_enabled(DMA_IRQ_1, true);
	dma_set_irq1_channel_mask_enabled(1U << chan32, true);

	srand((unsigned) to_us_since_boot(get_absolute_time()));

	stdio_usb_init();
	/* Wait until someone is connected. */
	while (!tud_cdc_connected())
		sleep_ms(100);

	puts("xfer_bytes,memcpy_us,dma8_us,dma32_us");
	for (int runs = 0; runs < TEST_RUNS; runs++)
		for (int i = 2; i <= MAX_SCALE; i++) {
			int64_t memcpy_us, dma8_us, dma32_us;
			int xfer = 1U << i;

			overwrite(xfer);
			from = get_absolute_time();
			memcpy(dst, src, xfer);
			to = get_absolute_time();
			if (!check(xfer)) {
				puts("memcpy error");
				exit (-1);
			}
			memcpy_us = absolute_time_diff_us(from, to);

			overwrite(xfer);
			dma_channel_set_read_addr(chan8, src, false);
			dma_channel_set_write_addr(chan8, dst, false);
			dma_channel_set_trans_count(chan8, xfer, false);
			dma_done = false;
			from = get_absolute_time();
			dma_channel_start(chan8);
			while (!dma_done)
				__wfi();
			/* to time set in the ISR */
			if (!check(xfer)) {
				puts("DMA 8 error");
				exit (-1);
			}
			dma8_us = absolute_time_diff_us(from, dma_to);

			overwrite(xfer);
			dma_channel_set_read_addr(chan32, src, false);
			dma_channel_set_write_addr(chan32, dst, false);
			dma_channel_set_trans_count(chan32, xfer >> 2, false);
			dma_done = false;
			from = get_absolute_time();
			dma_channel_start(chan32);
			while (!dma_done)
				__wfi();
			/* to time set in the ISR */
			if (!check(xfer)) {
				puts("DMA 32 error");
				exit (-1);
			}
			dma32_us = absolute_time_diff_us(from, dma_to);

			printf("%d,%lld,%lld,%lld\n", xfer, memcpy_us, dma8_us,
			       dma32_us);
		}

	for (;;)
		__wfi();
}
